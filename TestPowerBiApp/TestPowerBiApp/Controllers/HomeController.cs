﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using  System.Threading.Tasks;
using Microsoft.Rest;
using  TestPowerBiApp.Models;
using Microsoft.PowerBI.Api.Models;
using Microsoft.PowerBI.Api;
using Microsoft.PowerBI.Api.V2;

namespace TestPowerBiApp.Controllers
{
    public class HomeController : Controller
    {
        private  PowerbiSettings _powerbiSettings =  new PowerbiSettings();

        private async Task<AuthenticationResult> Authenticate()
        {
            var credential = new UserPasswordCredential(_powerbiSettings.Username,_powerbiSettings.Password);
            var authenticationContext = new AuthenticationContext(_powerbiSettings.AuthorityUrl);
            var authenticationResult =
                await authenticationContext.AcquireTokenAsync(_powerbiSettings.ResourceUrl, _powerbiSettings.ApplicationId, credential);
            return authenticationResult;
        }


        private async Task<TokenCredentials> CreateCredentials()
        {
            AuthenticationResult authenticationResult = await Authenticate();
            if (authenticationResult == null)
            {
                return null;
            }
            TokenCredentials tokenCredentials = new TokenCredentials(authenticationResult.AccessToken, "Bearer");
            return tokenCredentials;
        }

        public ActionResult Index()
        {
            return View();
        }


        public async Task<ActionResult> GetReports(string workspaceId)
        {
            Guid workspaceIdGuid = Guid.Empty;

            if (string.IsNullOrWhiteSpace(workspaceId))
            {
                 workspaceIdGuid = Guid.Parse(_powerbiSettings.WorkspaceId);
            }
            try
            {
                TokenCredentials tokenCredentials = await CreateCredentials();
                if (tokenCredentials == null)
                {
                    var error = "Authentication Failed";
                    return Json(error, JsonRequestBehavior.AllowGet);
                }
                using (var client = new PowerBIClient(new Uri(_powerbiSettings.ApiUrl), tokenCredentials))
                {
                    var reports = await client.Reports.GetReportsInGroupAsync(workspaceIdGuid);
                    return Json(reports.Value, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> EmbedReport(string reportId, string workspaceId)
        {

            Guid reportIdGuid = Guid.Empty;
            Guid workspaceIdGuid = Guid.Empty;

            if (string.IsNullOrWhiteSpace(reportId))
            {
                reportIdGuid = Guid.Parse(_powerbiSettings.ReportId);
            }

            if (string.IsNullOrWhiteSpace(workspaceId))
            {
                workspaceIdGuid = Guid.Parse( _powerbiSettings.WorkspaceId);
            }
            var result = new EmbedConfig();
            try
            {
                TokenCredentials tokenCredentials = await CreateCredentials();
                if (tokenCredentials == null)
                {
                    result.ErrorMessage = "Authentication Failed.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                using (var client = new PowerBIClient(new Uri(_powerbiSettings.ApiUrl), tokenCredentials))
                {

                    GenerateTokenRequest generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "view");

                    Report report = await client.Reports.GetReportInGroupAsync(workspaceIdGuid, reportIdGuid);

                    var tokenResponse = await client.Reports.GenerateTokenInGroupAsync(workspaceIdGuid, report.Id, generateTokenRequestParameters);

                    if (tokenResponse == null)
                    {
                        result.ErrorMessage = "Failed to generate embed token.";

                        return Json(result, JsonRequestBehavior.AllowGet);
                    }

                    result.EmbedToken = tokenResponse;
                    result.EmbedUrl = report.EmbedUrl;
                    result.Id = report.Id.ToString();

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                result.ErrorMessage = ex.Message;

                return Json(result, JsonRequestBehavior.AllowGet);

            }
        }
    }
}